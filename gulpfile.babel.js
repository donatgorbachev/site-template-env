const gulp             = require('gulp')
const gutil            = require('gulp-util')
const webpackStream    = require('webpack-stream')
const webpack          = require('webpack')
const WebpackDevServer = require('webpack-dev-server')


gulp.task('default', ['webpack:dev-server'])

gulp.task('build', ['webpack:build'])

gulp.task('webpack:build', () => {
  process.env.NODE_ENV = 'production'
  return gulp.src('assets/main.js')
    .pipe(webpackStream(require('./webpack.config'), webpack))
    .pipe(gulp.dest('project/project/static'))

})

gulp.task('webpack:dev-server', () => {
  process.env.NODE_ENV = 'development'
  const conf = require('./webpack.config')
  const port = conf.devServer.port
  return new WebpackDevServer(webpack(conf), {
    stats: {
      colors: true
    }
  }).listen(port, 'localhost', err => {
    if (err) throw new gutil.PluginError('webpack-dev-server', err)
    gutil.log('[webpack-dev-server]',
              `http://localhost:${port}/webpack-dev-server/index.html`)
  })
})
